require "http/client"
require "json"

stories_length = 10

class Story
  include JSON::Serializable

  @[JSON::Field(key: "title")]
  property title : String

  @[JSON::Field(key: "url")]
  property url : String

  @[JSON::Field(key: "score")]
  property score : Int32

  @[JSON::Field(key: "by")]
  property by : String
end

response = HTTP::Client.get "https://hacker-news.firebaseio.com/v0/topstories.json"

ids = Array(Int32).from_json(response.body)
top_five = ids[0..stories_length]

channel = Channel(Story).new(stories_length)
top_five.each { |id|
  spawn do
    res = HTTP::Client.get "https://hacker-news.firebaseio.com/v0/item/#{id}.json"
    story = Story.from_json(res.body)
    channel.send(story)
  end
}

stories_length.times do
  story = channel.receive
  puts "#{story.title}\nScore: #{story.score}\nBy: #{story.by}\nURL: #{story.url}\n\n"
end
